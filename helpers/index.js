const co = require('co')
const jwt = require('jsonwebtoken')
const config = require('config')
const _ = require('lodash')
const errors = require('./errors')

/**
 * Runs a function using the co model so we can use
 * async/await syntax
 * @param {Function} fn The controller function
 */
function wrapRoute (fn) {
  return (req, res, next) => {
    co(fn(req, res, next)).catch(next)
  }
}

/**
 * Generate a JWT
 * @param {String} role the user role
 */
async function generateToken (role) {
  return jwt.sign({ role }, config.JWT_SECRET)
}

/**
 * Decoded a JWT
 * @param {String} token the JWT
 * @param {Array} roles the array of allowed roles
 */
async function validateToken (token, roles) {
  const decoded = await jwt.verify(token, config.JWT_SECRET)
  if (!_.isUndefined(roles)) {
    if (!roles.includes(decoded.role)) {
      throw new errors.ForbiddenError('You are not allowed to perform this operation')
    }
  }
  return decoded
}

module.exports = {
  wrapRoute,
  generateToken,
  validateToken
}