const service = require('../services/AchievementType')
const { wrapRoute } = require('../helpers')

/**
 * Get all achievement types
 * @param {Object} req the request object
 * @param {Object} res the response object
 */
async function getAll (req, res) {
  res.json(await service.getAll())
}

/**
 * Create an achievement type
 * @param {Object} req the request object
 * @param {Object} res the response object
 */
async function create (req, res) {
  res.json(await service.create(req.body))
}

/**
 * Get single achievement type
 * @param {Object} req the request object
 * @param {Object} res the response object
 */
async function getSingle (req, res) {
  const doc = await service.getSingle(req.params.id)
  if (!doc) {
    return res.sendStatus(404)
  }
  res.json(doc)
}

/**
 * Get single achievement type
 * @param {Object} req the request object
 * @param {Object} res the response object
 */
async function update (req, res) {
  try {
    res.json(await service.update(req.params.id, req.body))
  } catch (e) {
    res.sendStatus(404)
  }
}

/**
 * Remove an achievement type
 * @param {Object} req the request object
 * @param {Object} res the response object
 */
async function remove (req, res) {
  try {
    await service.remove(req.params.id)
    res.sendStatus(204)
  } catch (e) {
    res.sendStatus(404)
  }
}

module.exports = {
  getAll: wrapRoute(getAll),
  create: wrapRoute(create),
  getSingle: wrapRoute(getSingle),
  update: wrapRoute(update),
  remove: wrapRoute(remove)
}