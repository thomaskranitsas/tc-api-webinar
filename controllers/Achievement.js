const service = require('../services/Achievement')
const { wrapRoute } = require('../helpers')

/**
 * Get all achievement
 * @param {Object} req the request object
 * @param {Object} res the response object
 */
async function getAll (req, res) {
  res.json(await service.getAll(req.params.memberId))
}

/**
 * assign achievement to a member
 * @param {Object} req the request object
 * @param {Object} res the response object
 */
async function assignAchievement (req, res) {
  try {
    res.json(await service.assignAchievement(req.params.memberId, req.body.achievementId))
  } catch (e) {
    res.sendStatus(400)
  }
}

/**
 * Remove achievement to a member
 * @param {Object} req the request object
 * @param {Object} res the response object
 */
async function removeAchievement (req, res) {
  try {
    await service.removeAchievement(req.params.memberId, req.params.achievementId)
    res.sendStatus(204)
  } catch (e) {
    res.sendStatus(400)
  }
}

module.exports = {
  getAll: wrapRoute(getAll),
  assignAchievement: wrapRoute(assignAchievement),
  removeAchievement: wrapRoute(removeAchievement)
}