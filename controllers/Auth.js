const { wrapRoute } = require('../helpers')
const { generateToken } = require('../helpers')

/**
 * Mock login endpoint
 * @param {Object} req the request object
 * @param {Object} res the response object
 */
async function login (req, res) {
  const token = await generateToken(req.params.role)
  res.json({ token })
}

module.exports = {
  login: wrapRoute(login)
}