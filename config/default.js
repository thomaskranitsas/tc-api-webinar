module.exports = {
  JWT_SECRET: process.env.JWT_SECRET || 'secret',
  LOG_LEVEL: process.env.LOG_LEVEL || 'debug',
  PORT: process.env.PORT || 3000,
  MONGODB_URI: process.env.MONGODB_URI || 'mongodb://localhost:27017/webinar'
}