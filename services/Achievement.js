const _ = require('lodash')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const Achievement = require('../models/Achivement')
const AchievementType = require('../models/AchivementType')
const logger = require('../helpers/logger')

/**
 * Get all achievements
 * @param {Number} memberId the member ID
 */
async function getAll (memberId) {
  return await Achievement.find({ memberId })
}

getAll.schema = {
  memberId: Joi.number().required()
}

/**
 * Assign an achievement to a member
 * @param {Number} memberId The member Id
 * @param {String} achievementId The achievement ID
 */
async function assignAchievement (memberId, achievementId) {
  const achivementExists = await AchievementType.findById(achievementId)
  if (!achivementExists) throw new Error()
  const alreadyAssigned = await Achievement.findOne({ memberId, achievementId })
  if (alreadyAssigned) throw new Error()
  return await Achievement.create({
    memberId: _.toInteger(memberId),
    achievementId
  })
}

assignAchievement.schema = {
  memberId: Joi.number().required(),
  achievementId: Joi.objectId().required()
}

/**
 * Delete an achievement from a member
 * @param {Number} memberId The member Id
 * @param {String} achievementId The achievement ID
 */
async function removeAchievement (memberId, achievementId) {
  const existing = await Achievement.findOne({ memberId, achievementId })
  if (!existing) throw new Error()
  return await existing.remove()
}

removeAchievement.schema = {
  memberId: Joi.number().required(),
  achievementId: Joi.string().required()
}

module.exports = {
  getAll,
  assignAchievement,
  removeAchievement
}

logger.autoValidate(module.exports)