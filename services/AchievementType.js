const _ = require('lodash')
const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const AchievementType = require('../models/AchivementType')
const logger = require('../helpers/logger')

/**
 * Get all achievement types
 */
async function getAll () {
  return await AchievementType.find({})
}

/**
 * Create a new achivement type
 * @param {Object} data The achievement type object
 */
async function create (data) {
  return await AchievementType.create(data)
}

create.schema = {
  data: Joi.object().keys({
    name: Joi.string().required()
  }).required()
}

/**
 * Get a single achievement type based on the id
 * @param {String} id 
 */
async function getSingle (id) {
  return await AchievementType.findById(id)
}

getSingle.schema = {
  id: Joi.objectId().required()
}

/**
 * Update a single achievement type
 * @param {String} id 
 * @param {Object} updatedData 
 */
async function update (id, updatedData) {
  const doc = await AchievementType.findById(id)
  if (!doc) throw new Error()
  _.extend(doc, _.omit(updatedData, ['_id']))
  return await doc.save()
}

update.schema = {
  id: Joi.objectId().required(),
  updatedData: Joi.object().keys({
    name: Joi.string().required()
  }).required()
}

/**
 * Remove a single achievement type
 * @param {String} id 
 */
async function remove (id) {
  const doc = await AchievementType.findById(id)
  if (!doc) throw new Error()
  return await doc.remove()
}

remove.schema = {
  id: Joi.objectId().required()
}

module.exports = {
  getAll,
  create,
  getSingle,
  update,
  remove
}

logger.autoValidate(module.exports)
