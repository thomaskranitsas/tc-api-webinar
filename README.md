# Webinar - API development using Node.js

## System requirements

- [NodeJS](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/)
- [Postman](https://www.postman.com/) - for verification
- [Swagger Editor](https://editor.swagger.io/)

## Configuration

- All configurations can be found in `./config/default.js`

## Local deployment

- Run `mongod` to start the MongoDB. Leave this terminal window open and 
- Run `npm i` to install the dependencies
- Run `npm start` to start the API
