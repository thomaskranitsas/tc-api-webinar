const mongoose = require('mongoose')

const { Schema } = mongoose

const AchivementSchema = new Schema({
  memberId: { type: Number, required: true },
  achievementId: { type: String, required: true }
})

module.exports = mongoose.model('Achivement', AchivementSchema)