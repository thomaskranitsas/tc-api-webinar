FROM node:10

ADD . .

RUN npm i

EXPOSE 8080

CMD ["npm", "start"]