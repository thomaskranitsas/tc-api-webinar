const achievementTypeController = require('./controllers/AchivementType')
const achievementController = require('./controllers/Achievement')
const authController = require('./controllers/Auth')

module.exports = {
  '/login/:role': {
    get: {
      method: authController.login
    }
  },
  '/achievement-types': {
    get: {
      method: achievementTypeController.getAll,
      requiresAuth: true,
      allowedRoles: ['admin']
    },
    post: {
      method: achievementTypeController.create,
      requiresAuth: true,
      allowedRoles: ['admin']
    }
  },
  '/achievement-types/:id': {
    get: {
      method: achievementTypeController.getSingle
    },
    put: {
      method: achievementTypeController.update
    },
    delete: {
      method: achievementTypeController.remove
    }
  },
  '/members/:memberId/achievements': {
    get: {
      method: achievementController.getAll
    },
    post: {
      method: achievementController.assignAchievement
    }
  },
  '/members/:memberId/achievements/:achievementId': {
    delete: {
      method: achievementController.removeAchievement
    }
  }
}