const express = require('express')
const config = require('config')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const cors = require('cors')
const _ = require('lodash')
const { validateToken } = require('./helpers')
const { errorHandler } = require('./helpers/middlewares')
const errors = require('./helpers/errors')
const routes = require('./routes')

// Connect to MongoDB
mongoose.connect(config.MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
  .then(() => console.log('Connected to DB'))
  .catch((e) => {
    console.log('Failed to connect to DB')
    console.log(e)
    process.exit(1)
  })

const app = express()

// Add required middlewares
app.use(bodyParser.json())
app.use(cors())

// load routes
_.each(routes, (route, routeDef) => {
  _.each(route, (def, verb) => {
    const actions = []

    if (def.requiresAuth) {
      actions.push(async (req, res, next) => {
        try {
          const token = req.get('authorization')
          if (!token) throw new errors.UnauthorizedError('Action not allowed for anonymous')
          const authUser = await validateToken(_.get(token.split(' '), '[1]'), def.allowedRoles)
          req.authUser = authUser
          next()
        } catch (e) {
          next(e)
        }
      })
    }
    
    actions.push(def.method)
    app[verb](routeDef, actions)
  })
})

app.use(errorHandler)

// Start the API
app.listen(config.PORT, () => console.log(`API is running on port ${config.PORT}`))